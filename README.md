# Pure Docker

GNU/Linux for Android,Termux,Kindle,Kobo,...

## Status

WIP

## Install

```bash
curl https://gitlab.com/zaoqi/pure-docker/-/archive/master/pure-docker-master.tar.gz 2>/dev/null | tar -xzv
```

## Example

```bash
./make-rootfs-dir-targz.sh rootfs $(./get-alpine-targz.sh armhf)
cd rootfs
../start-chroot-dir.sh sh -l
```
