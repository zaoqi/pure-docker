#!/bin/sh
# $PWD dir
# $* command

. "$(dirname "$0")/libcommon.sh"

LinkDir(){
    # $1 src
    # $2 dest
    mkdir -p "$2" ||return
    Mount --bind "$1" "$2" ||return
}
LinkFile(){
    # $1 src
    # $2 dest
    touch "$2" ||return
    Mount --bind "$1" "$2" ||return
}

ck="$(mktemp)"
if [ ! -f "./tmp/$(basename "$ck")" ] ;then
    LinkDir /dev ./dev ||exit
    Mount --bind /dev/pts ./dev/pts ||exit
    LinkDir /proc ./proc ||exit
    LinkDir /sys ./sys ||exit
    LinkDir "$(dirname "$ck")" ./tmp ||exit

    LinkDir / ./host ||exit
    LinkFile /etc/resolv.conf ./etc/resolv.conf ||exit
fi
rm "$ck"
exec chroot . /bin/sh -c "$*"
