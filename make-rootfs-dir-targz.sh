#!/bin/sh
# $1 dir
# $2 url

. "$(dirname "$0")/libcommon.sh"

mkdir -p "$1"
Get "$2" | Tar -xzv -C "$1"
