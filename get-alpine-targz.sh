#!/bin/sh
# $1 arch

. "$(dirname "$0")/libcommon.sh"

Get https://alpinelinux.org/downloads/ | grep 'minirootfs-[^-]*-'"$1"'.tar.gz"' | sed 's|&#x2F;|/|g' | sed 's|^.*="\(http[^"]*.tar.gz\)".*$|\1|g'
