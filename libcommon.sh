#!/bin/sh
HasCmd(){
    type "$1" >/dev/null 2>/dev/null
}
Get(){
    if HasCmd curl ;then
	curl "$1" 2>/dev/null
    elif HasCmd busybox ;then
	busybox wget -O - "$1" 2>/dev/null
    elif HasCmd wget ;then
	wget -O - "$1" 2>/dev/null
    else
	return 1
    fi
}
Tar(){
    if HasCmd busybox ;then
	busybox tar "$@"
    else
	tar "$@"
    fi
}
Mount(){
    mount "$@"
}
